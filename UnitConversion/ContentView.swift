//
//  ContentView.swift
//  UnitConversion
//
//  Created by Vrushali Kulkarni on 15/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import SwiftUI

struct ContentView: View {

    @State private var tempSelectedInputUnit = 0
    @State private var tempSelectedOutputUnit = 2

    @State private var lengthSelectedInputUnit = 1
    @State private var lengthSelectedOutputUnit = 3

    @State private var timeSelectedInputUnit = 2
    @State private var timeSelectedOutputUnit = 3

    @State private var volumeSelectedInputUnit = 0
    @State private var volumeSelectedOutputUnit = 1

    @State private var tempInputValue = ""
    @State private var lengthInputValue = ""
    @State private var timeInputValue = ""
    @State private var volumeInputValue = ""

    let temperatureUnit = ["Celsius", "Fahrenheit", "Kelvin"]
    let lengthUnit = ["km", "m", "miles", "yards", "feet"]
    let timeUnit = ["days", "hours", "minutes", "seconds"]
    let volumeUnit = ["gallons", "l", "pints", "cups", "ml"]

    var convertedTemperatureResult: Double {

        let input = Double(tempInputValue) ?? 0
        var kelvinConversion: Double
        var finalConversion: Double

        if tempSelectedInputUnit == 0 {
            kelvinConversion = input + 273.15
        } else if tempSelectedInputUnit == 1 {
            kelvinConversion = ((input - 32) * 5/9) + 273.15
        } else {
            kelvinConversion = input
        }

        if tempSelectedOutputUnit == 0 {
            finalConversion = kelvinConversion - 273.15
        } else if tempSelectedOutputUnit == 1 {
            finalConversion = ((kelvinConversion - 273.15) * 9/5) + 32
        } else {
            finalConversion = kelvinConversion
        }
        return finalConversion
    }

    var convertedLengthResult: Double {

        let input = Double(lengthInputValue) ?? 0
        var yardConversion: Double
        var finalConversion: Double

        if lengthSelectedInputUnit == 0 {
            yardConversion = input * 3281
        } else if lengthSelectedInputUnit == 1 {
            yardConversion = input * 3.281
        } else if lengthSelectedInputUnit == 2 {
            yardConversion = input * 5280
        } else if lengthSelectedInputUnit == 3 {
            yardConversion = input * 3
        } else {
            yardConversion = input
        }

        if lengthSelectedOutputUnit == 0 {
            finalConversion = yardConversion/3281
        } else if lengthSelectedOutputUnit == 1 {
            finalConversion = yardConversion/3.281
        } else if lengthSelectedOutputUnit == 2 {
            finalConversion = yardConversion/5280
        } else if lengthSelectedOutputUnit == 3 {
            finalConversion = yardConversion/3
        } else {
            finalConversion = yardConversion
        }

        return finalConversion
    }

    var convertedTimeResult: Double {

        let input = Double(timeInputValue) ?? 0
        var secondsConversion: Double
        var finalConversion: Double

        if timeSelectedInputUnit == 0 {
            secondsConversion = input * 86400
        } else if timeSelectedInputUnit == 1 {
            secondsConversion = input * 3600
        } else if timeSelectedInputUnit == 2 {
            secondsConversion = input * 60
        } else {
            secondsConversion = input
        }

        if timeSelectedOutputUnit == 0 {
            finalConversion = secondsConversion/86400
        } else if timeSelectedOutputUnit == 1 {
            finalConversion = secondsConversion/3600
        } else if timeSelectedOutputUnit == 2 {
            finalConversion = secondsConversion/60
        } else {
            finalConversion = secondsConversion
        }

        return finalConversion
    }

    var convertedVolumeResult: Double {

        let input = Double(volumeInputValue) ?? 0
        var millilitreConversion: Double
        var finalConversion: Double

        if volumeSelectedInputUnit == 0 {
            millilitreConversion = input * 3785
        } else if volumeSelectedInputUnit == 1 {
            millilitreConversion = input * 1000
        } else if volumeSelectedInputUnit == 2 {
            millilitreConversion = input * 473.176
        } else if volumeSelectedInputUnit == 3 {
            millilitreConversion = input * 240
        } else {
            millilitreConversion = input
        }

        if volumeSelectedOutputUnit == 0 {
            finalConversion = millilitreConversion/3785
        } else if volumeSelectedOutputUnit == 1 {
            finalConversion = millilitreConversion/1000
        } else if volumeSelectedOutputUnit == 2 {
            finalConversion = millilitreConversion/473.176
        } else if volumeSelectedOutputUnit == 3 {
            finalConversion = millilitreConversion/240
        } else {
            finalConversion = millilitreConversion
        }

        return finalConversion
    }

    var body: some View {

        Form {

            Section(header: Text("Temperature Conversion")) {

                Picker("Input Unit", selection: $tempSelectedInputUnit) {
                    ForEach(0..<temperatureUnit.count) {
                        Text("\(self.temperatureUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                Picker("Output Unit", selection: $tempSelectedOutputUnit) {
                    ForEach(0..<temperatureUnit.count) {
                        Text("\(self.temperatureUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                TextField("Enter Value", text: $tempInputValue)
                    .keyboardType(.decimalPad)

                Text("Converted Value: \(convertedTemperatureResult, specifier: "%.2f")")
            }

            Section(header: Text("Length Conversion")) {

                Picker("Input Unit", selection: $lengthSelectedInputUnit) {
                    ForEach(0..<lengthUnit.count) {
                        Text("\(self.lengthUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                Picker("Output Unit", selection: $lengthSelectedOutputUnit) {
                    ForEach(0..<lengthUnit.count) {
                        Text("\(self.lengthUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                TextField("Enter Value", text: $lengthInputValue)
                .keyboardType(.decimalPad)

                Text("Converted Value: \(convertedLengthResult, specifier: "%.2f")")
            }

            Section(header: Text("Time Selection")) {
                Picker("Input Unit", selection: $timeSelectedInputUnit) {
                    ForEach(0..<timeUnit.count) {
                        Text("\(self.timeUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                Picker("Output Unit", selection: $timeSelectedOutputUnit) {
                    ForEach(0..<timeUnit.count) {
                        Text("\(self.timeUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                TextField("Enter Value", text: $timeInputValue)
                .keyboardType(.numberPad)

                Text("Converted Value: \(convertedTimeResult)")
            }

            Section(header: Text("Volume Conversion")) {
                Picker("Input Unit", selection: $volumeSelectedInputUnit) {
                    ForEach(0..<volumeUnit.count) {
                        Text("\(self.volumeUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                Picker("Output Unit", selection: $volumeSelectedOutputUnit) {
                    ForEach(0..<volumeUnit.count) {
                        Text("\(self.volumeUnit[$0])")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())

                TextField("Enter Value", text: $volumeInputValue)
                .keyboardType(.decimalPad)

                Text("Converted Value: \(convertedVolumeResult)")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
